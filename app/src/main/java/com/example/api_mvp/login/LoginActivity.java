package com.example.api_mvp.login;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.api_mvp.R;
import com.example.api_mvp.databinding.ActivityLoginBinding;
import com.example.api_mvp.models.User;

public class LoginActivity extends AppCompatActivity implements LoginInterface.LoginView, View.OnClickListener {

    LoginPresenter loginPresenter;
    ActivityLoginBinding viewActivityLoginBinding;

    User user;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewActivityLoginBinding  = DataBindingUtil.setContentView(this,R.layout.activity_login);

        loginPresenter =new LoginPresenter(this);

       viewActivityLoginBinding.sign.setOnClickListener(this);

       user = new User();

    }


    @Override
    public void onSuccsees(String message) {

        String username = user.getFull_name();
        Toast.makeText(this,message,Toast.LENGTH_LONG).show();
        //Log.i("username"," " +username);
    }

    @Override
    public void onFialre(String message) {
        Toast.makeText(getBaseContext(),message,Toast.LENGTH_LONG).show();


      //  viewActivityLoginBinding.j.setText("jjjj");

    }

    @Override
    public void onClick(View v) {


        String email = viewActivityLoginBinding.email.getText().toString();
        String pass = viewActivityLoginBinding.password.getText().toString();

        if(email.isEmpty()){
            viewActivityLoginBinding.email.setError("Please fill in");
        } else if(pass.isEmpty()){
            viewActivityLoginBinding.password.setError("Please fill in");
        }else {

            loginPresenter.login(email, pass , "0" , "" ,"", "","en");
        }
    }
}