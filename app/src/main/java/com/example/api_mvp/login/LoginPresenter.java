package com.example.api_mvp.login;

import android.util.Log;

import com.example.api_mvp.network.ApiClient;
import com.example.api_mvp.network.ApiService;
import com.example.api_mvp.network.responce.LoggedInResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginPresenter implements LoginInterface.LoginPresenter {


    private ApiService apiService = ApiClient.getClient().create(ApiService.class);

    LoginInterface.LoginView loginView;

    public LoginPresenter(LoginInterface.LoginView loginView) {
        this.loginView = loginView;
    }

    @Override
    public void login(String email, String password, String type, String fcn,
                      String full_name, String socail_id, String lang) {


        Call<LoggedInResponse> logged = apiService.login(email, password, type, fcn,
                full_name, socail_id , lang);


        logged.enqueue(new Callback<LoggedInResponse>() {
            @Override
            public void onResponse(Call<LoggedInResponse> call, Response<LoggedInResponse> response) {

                if (response.isSuccessful()) {
                    if (response.body() != null)
                    if (response.body().getResult().getStatus().equalsIgnoreCase("1")){
                        loginView.onSuccsees(response.body().getResult().getMessage());
                        Log.e("hh",response.body().getResult().getMessage());
                        Log.e("username",response.body().getUser().getFull_name());
                    }
                    else {
                        loginView.onFialre(response.body().getResult().getMessage());
                    }
                }

            }

            @Override
            public void onFailure(Call<LoggedInResponse> call, Throwable t) {
                loginView.onFialre(t.getLocalizedMessage());
            }
        });

    }
}
