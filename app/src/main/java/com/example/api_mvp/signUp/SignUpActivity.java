package com.example.api_mvp.signUp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;


import com.example.api_mvp.R;
import com.example.api_mvp.databinding.ActivitySignUpBinding;

public class SignUpActivity extends AppCompatActivity implements SingUpInterface.signUpView {


    ActivitySignUpBinding view;

    String text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        view = DataBindingUtil.setContentView(this, R.layout.activity_sign_up);

        SignUpPresenter signUpPresenter = new SignUpPresenter(this);

        view.s.setOnClickListener(v -> {
            text = view.e.getText().toString();
            signUpPresenter.signUp(text);
        });
    }

    @Override
    public void onSuccess() {

        view.s.setText("1");
    }

    @Override
    public void onFailure() {

        view.s.setText("2");
    }
}