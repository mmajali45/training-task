package com.example.api_mvp.signUp;

import java.util.Objects;

class SignUpPresenter implements SingUpInterface.SignUpPresenter {
    SingUpInterface.signUpView signUpView;

    public SignUpPresenter(SingUpInterface.signUpView signUpView) {
        this.signUpView = signUpView;
    }

    @Override
    public void signUp(String text) {

        if (Objects.equals(text, "he")) {
            signUpView.onSuccess();
        } else {
            signUpView.onFailure();
        }
    }
}
