package com.example.api_mvp.signUp;

public interface SingUpInterface {

    interface signUpView{

        void onSuccess();
        void onFailure();
    }

    interface SignUpPresenter{

        void signUp(String text);
    }
}
