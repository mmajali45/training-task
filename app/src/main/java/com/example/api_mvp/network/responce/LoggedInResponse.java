package com.example.api_mvp.network.responce;

import com.example.api_mvp.models.Result;
import com.example.api_mvp.models.User;
import com.google.gson.annotations.SerializedName;

public class LoggedInResponse {

    @SerializedName("result")
    private Result result;

    @SerializedName("data")
    private User user;

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
