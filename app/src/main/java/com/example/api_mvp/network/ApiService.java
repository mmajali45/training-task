package com.example.api_mvp.network;


import com.example.api_mvp.network.responce.LoggedInResponse;


import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;


public interface ApiService {

    @FormUrlEncoded
    @POST("api/login")
    Call<LoggedInResponse> login(@Field("email") String email,
                                 @Field("password") String password,
                                 @Field("login_type") String type,
                                 @Field("fcn_token") String fcn,
                                 @Field("full_name") String full_name,
                                 @Field("social_id") String socail_id,
                                 @Field("lang")  String lang);


}
